package edu.upc.damo.punts_mvc;

import android.util.Log;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Josep M on 30/09/13.
 */
public class CjtDePunts implements Iterable<Punt> {
    static final String TAG = "PROVA";
    /* Magatzem de dades */
    private final List<Punt> punts = new LinkedList<Punt>();
    /* Observador */
    private CanviCjtDePuntsListener observador;

    /* --------------- Mètodes públics ------------------------- */
    public CjtDePunts() {
    }

    public void setCjtDePuntsListener(CanviCjtDePuntsListener o) {
        observador = o;
    }


    /* Enregistrament de l'observador */

    public void afegeixPunt(Punt p) {
        punts.add(p);
        avisaObservador();
    }

    private void avisaObservador() {
        Log.i(TAG, "Avisaobservador");

        if (observador != null) {
            observador.onCanviCjtDePunts();
        }
    }


    /* --------------- Mètodes privats ------------------------- */

    @Override
    public Iterator<Punt> iterator() {
        return punts.listIterator();

    }

    /* Interfície que han d'implementar els observadors */
    public interface CanviCjtDePuntsListener {
        void onCanviCjtDePunts();
    }


}
