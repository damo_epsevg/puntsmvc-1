package edu.upc.damo.punts_mvc;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Josep M on 30/09/13.
 */
public class VistaDePunts extends View
        implements CjtDePunts.CanviCjtDePuntsListener {

    private CjtDePunts punts;


    public VistaDePunts(Context context) {
        super(context);
        setFocusableInTouchMode(true);


    }

    /**
     * @param context
     * @param attrs
     */
    public VistaDePunts(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusableInTouchMode(true);
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public VistaDePunts(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusableInTouchMode(true);
    }

    public void defineixModel(CjtDePunts punts) {
        this.punts = punts;
        punts.setCjtDePuntsListener(this);
    }

    /**
     * /**
     *
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(hasFocus() ? Color.BLUE : Color.GRAY);

        canvas.drawRect(0, 0, getWidth() - 1, getHeight() - 1, paint);

        if (null == punts) {
            return;
        }


        paint.setStyle(Paint.Style.FILL);
        for (Punt dot : punts) {
            paint.setColor(dot.getColor());
            canvas.drawCircle(
                    dot.getX(),
                    dot.getY(),
                    dot.getDiametre(),
                    paint);

        }

    }

    @Override
    public void onCanviCjtDePunts() {
        invalidate();
    }
}