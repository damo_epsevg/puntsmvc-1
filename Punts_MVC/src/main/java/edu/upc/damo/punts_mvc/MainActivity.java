package edu.upc.damo.punts_mvc;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends Activity {

    static final String TAG = "PROVA";
    static final int DIAMETRE = 25;

    private TextView t1;
    private TextView t2;
    private Random generador = new Random();

    private CjtDePunts cjtDePunts = new CjtDePunts();       // El Model
    private VistaDePunts vista;                            // La vista


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();

        vista.defineixModel(cjtDePunts);
    }

    /* Funcions auxiliars sobre la generació i visualització de punts */

    private void mostraCoordenades(Punt punt) {
        t1.setText(String.valueOf(punt.getX()));
        t2.setText(String.valueOf(punt.getY()));
    }

    private void afegeixPunt(MotionEvent event, int c) {
        Punt punt = new Punt(event.getX(), event.getY(), c, DIAMETRE);
        mostraCoordenades(punt);
        cjtDePunts.afegeixPunt(punt);
    }

    private void afegeixPunt(int c) {
        Punt puntAleatori = generaPunt(c);
        mostraCoordenades(puntAleatori);
        cjtDePunts.afegeixPunt(puntAleatori);
    }

    private Punt generaPunt(int c) {
        return new Punt(generador.nextFloat() * vista.getWidth() + DIAMETRE,
                generador.nextFloat() * vista.getHeight() + DIAMETRE, c, DIAMETRE);
    }



    /* Inicialització de l'activity */

    private void inicialitza() {
        t1 = (TextView) findViewById(R.id.text1);
        t2 = (TextView) findViewById(R.id.text2);
        vista = (VistaDePunts) findViewById(R.id.vistaDePunts);


        ((Button) findViewById(R.id.botoVerd)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "Captura listener del botó VERD");
                        afegeixPunt(Color.GREEN);
                    }
                }
        );

        ((Button) findViewById(R.id.botoVermell)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "Captura listener del botó VERMELL");
                        afegeixPunt(Color.RED);
                    }
                }
        );

        vista.setOnTouchListener(new View.OnTouchListener() {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         Log.i(TAG, "Captura listener del TECLAT" + event.getAction());

                                         if (MotionEvent.ACTION_DOWN != event.getAction())
                                             return false;

                                         afegeixPunt(Color.CYAN);
                                         return true;
                                     }
                                 }
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void testIteracio() {
        CjtDePunts c = new CjtDePunts();

        c.afegeixPunt(new Punt(10, 10, Color.BLUE, 2));
        c.afegeixPunt(new Punt(1, 1, Color.BLUE, 2));
        c.afegeixPunt(new Punt(13, 31, Color.BLUE, 1));

        for (Punt p : c) {
            Log.i(TAG, p.toString());
        }
    }

}
